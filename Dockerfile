ARG CENTOS_VERSION
FROM centos:${CENTOS_VERSION}
LABEL maintainer="Daniel von Essen"
ENV container=docker

# Install systemd -- See https://hub.docker.com/_/centos/
RUN yum -y update; yum clean all; \
  (cd /lib/systemd/system/sysinit.target.wants/; for i in *; do [ $i == systemd-tmpfiles-setup.service ] || rm -f $i; done); \
  rm -f /lib/systemd/system/multi-user.target.wants/*;\
  rm -f /etc/systemd/system/*.wants/*;\
  rm -f /lib/systemd/system/local-fs.target.wants/*; \
  rm -f /lib/systemd/system/sockets.target.wants/*udev*; \
  rm -f /lib/systemd/system/sockets.target.wants/*initctl*; \
  rm -f /lib/systemd/system/basic.target.wants/*;\
  rm -f /lib/systemd/system/anaconda.target.wants/*;

# Install requirements.
RUN yum -y install epel-release initscripts \
  && yum -y update \
  && yum -y install \
  sudo \
  which \
  python3-pip \
  && yum clean all

# Disable requiretty.
RUN sed -i -e 's/^\(Defaults\s*requiretty\)/#--- \1/'  /etc/sudoers

# Remove unnecessary getty and udev targets that can result in high CPU usage when using
# multiple containers with Molecule (https://github.com/ansible/molecule/issues/1104)
RUN rm -fr /lib/systemd/system/systemd*udev* \
  && rm -f /lib/systemd/system/getty.target

# Create `ansible` user with sudo permissions
RUN set -xe \
  && groupadd -r ansible \
  && useradd -m -g ansible ansible \
  && echo "%ansible ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/ansible

VOLUME ["/sys/fs/cgroup"]
CMD ["/usr/lib/systemd/systemd"]
